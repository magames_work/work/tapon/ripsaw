﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SawController : MonoBehaviour
{
    private LvlManager lvl;
    private UIManager ui;
    public GameObject object1, object2;
    public Material lineMat;
    void Start()
    {
        lvl = FindObjectOfType<LvlManager>();
        ui = lvl.GetComponent<UIManager>();
    }

    Vector3 dir;
    Quaternion rot;
    void FixedUpdate()
    {
        this.transform.position = 0.5f*(object1.transform.position + object2.transform.position);
        dir = object1.transform.position - transform.position;
        rot = Quaternion.LookRotation(dir);
        transform.rotation = rot;

        if (Vector3.Distance(object1.transform.position, object2.transform.position) >= 4)
        {
            this.transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, Vector3.Distance(object1.transform.position, object2.transform.position));
        }
    }

    //public Light _light;
    public GameObject shadowTarget;
    private void OnTriggerEnter(Collider other) 
    {
        if(!lvl.lvlFinished && other.transform.GetComponent<MeshFilter>().sharedMesh == lvl.lvls[lvl.currentLvl].obstacles[lvl.counter].GetComponent<MeshFilter>().sharedMesh)
        {
            if(float.Parse(ui.currentAngle.text) > float.Parse(ui.targetAngle.text) + 15 | float.Parse(ui.currentAngle.text) < float.Parse(ui.targetAngle.text) - 15)
            {
                ui.wrong.SetActive(true);
                lvl.wrongCuts++;
            }
            else
            {
                ui.perfect.SetActive(true);        
                lvl.perfectCuts++;
            }

            Color defColor = Color.red;

            if(lvl.lvls[lvl.currentLvl].obstacles.Length > lvl.counter + 1 && lvl.lvls[lvl.currentLvl].obstacles[lvl.counter + 1].GetComponent<ChangeLineColor>() != null)
            {
                lineMat.color = lvl.lvls[lvl.currentLvl].obstacles[lvl.counter + 1].GetComponent<ChangeLineColor>().color;
            }
            else
            {
                lineMat.color = defColor;
            }

            foreach(Material mat in other.GetComponent<MeshRenderer>().materials)
            {
                if(mat.name == "laser (Instance)")
                    Destroy(mat);
            }
            try{
                foreach(Material mat in other.transform.GetChild(0).GetComponent<MeshRenderer>().materials)
                {
                    if(mat.name == "laser" | mat.name == "laser (Instance)")
                        Destroy(other.transform.GetChild(0).gameObject);
                }
            }catch{}
            

            lvl.counter++;
            //Instantiate(shadowTarget, lvl.lvls[lvl.currentLvl].obstacles[lvl.counter].transform.position - Vector3.forward * 1.5f, Quaternion.Euler(lvl.lvls[lvl.currentLvl].targetsAngle[lvl.counter], -90, 0), lvl.lvls[lvl.currentLvl].obstacles[lvl.counter].transform);

            StartCoroutine("SlowMo");
        }
    }

    private IEnumerator SlowMo()
    {
            yield return new WaitForSecondsRealtime(0.1f);
        Time.timeScale = 0.2f;
            yield return new WaitForSecondsRealtime(1);
        Time.timeScale = 1f;
    }

    
}
