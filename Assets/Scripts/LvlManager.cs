﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LvlManager : MonoBehaviour
{
    private UIManager ui;
    private GameManager gm;

    
    public int Debuglvl;
    public int currentLvl;
    public float currentTarget;
    public GameObject currentObj;
    public int counter;
    public bool lvlFinished;
    public List<Lvls> lvls = new List<Lvls>();
    [Serializable]
    public class Lvls
    {
        public GameObject[] obstacles;
        public float[] targetsAngle;
    }

    public int totalStart, lvlStars, perfectCuts, wrongCuts;

    void Start()
    {
        Time.timeScale = 1;
        if(!PlayerPrefs.HasKey("currentLvl"))
        {
            currentLvl = 0;
            PlayerPrefs.SetInt("currentLvl", 0);
        }
        else
        {
            currentLvl = PlayerPrefs.GetInt("currentLvl");
            if(currentLvl > lvls.Count - 1)
            {
                currentLvl = 0;
                PlayerPrefs.SetInt("currentLvl", 0);
            }
            print(currentLvl);

        }

        if(Debuglvl != -1)
        {
            currentLvl = Debuglvl;
        }

        ui = GetComponent<UIManager>();
        gm = GetComponent<GameManager>();  

        counter = 0; 
        
        int i = 30;
        foreach(GameObject obj in lvls[currentLvl].obstacles)
        {
            Instantiate(obj, GameObject.Find("Obstacles").transform);
            i += 70;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(perfectCuts == 1)
            lvlStars = 1;
        else if(perfectCuts == 3)
            lvlStars = 2;
        else if(perfectCuts == 5)
            lvlStars = 3;
        
        if(counter >= lvls[currentLvl].obstacles.Length & !lvlFinished)
        {
            lvlFinished = true;
            Invoke("LateWin", 1);
            perfectCuts = 0;
            wrongCuts = 0;
            PlayerPrefs.SetInt("currentLvl", PlayerPrefs.GetInt("currentLvl") + 1);
        }

        if(!lvlFinished)
        {
            currentTarget = lvls[currentLvl].targetsAngle[counter];
            currentObj = lvls[currentLvl].obstacles[counter];
        }
    }
    public void LateWin()
    {
        ui.lvlCleared.SetActive(true);
    }

    public void LoadLvl(int lvl)
    {
        if(currentLvl == 1)
            SceneManager.LoadScene(1);
        else
            SceneManager.LoadScene(lvl);
    }

}
