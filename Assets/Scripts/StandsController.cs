﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandsController : MonoBehaviour
{
    public GameObject leftStand, rightStand;
    public float yMin, yMax, speed;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Mouse X") * speed * Time.deltaTime;
        
        if(leftStand.transform.position.y + x < yMax & leftStand.transform.position.y + x > yMin)
            leftStand.transform.position += Vector3.up * x;
        
        rightStand.transform.position = new Vector3(rightStand.transform.position.x, (yMax + yMin) - leftStand.transform.position.y, rightStand.transform.position.z);
    }
}
