﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private LvlManager lvl;
    private GameManager gm;

    public Text targetAngle, currentAngle, currentLvl, nextLvl;
    public GameObject lvlCleared, perfect, wrong;
    public Slider progressBar;
    public Image star1, star2, star3;
    public Sprite starDisable, starEnable;

    void Start()
    {
        lvl = GetComponent<LvlManager>();
        gm = GetComponent<GameManager>();        
        progressBar.maxValue = lvl.lvls[lvl.currentLvl].obstacles.Length;
    }


    Vector3 rot;
    private void Update() 
    {
        currentLvl.text = PlayerPrefs.GetInt("currentLvl").ToString();
        nextLvl.text = "" + (PlayerPrefs.GetInt("currentLvl") + 1) +"";

        progressBar.value = lvl.counter;

        rot = UnityEditor.TransformUtils.GetInspectorRotation(gm.saw.transform);
        targetAngle.text = lvl.currentTarget.ToString();
        currentAngle.text = rot.x.ToString();

        if(lvl.lvlStars == 0)
        {
            star1.sprite = starDisable;
            star2.sprite = starDisable;
            star3.sprite = starDisable;
        }
        if(lvl.lvlStars == 1)
        {
            star1.sprite = starEnable;
            star2.sprite = starDisable;
            star3.sprite = starDisable;
        }
        if(lvl.lvlStars == 2)
        {
            star1.sprite = starEnable;
            star2.sprite = starEnable;
            star3.sprite = starDisable;
        }
        if(lvl.lvlStars == 3)
        {
            star1.sprite = starEnable;
            star2.sprite = starEnable;
            star3.sprite = starEnable;
        }
    }
}
