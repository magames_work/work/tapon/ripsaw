﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private UIManager ui;
    private LvlManager lvl;

    public GameObject saw, targetSaw;

    void Start()
    {
        ui = GetComponent<UIManager>();
        lvl = GetComponent<LvlManager>();        
    }

    void Update()
    {
        targetSaw.transform.eulerAngles = new Vector3(lvl.currentTarget, -90, 0);
    }
}
